# zst - simple terminal
# See LICENSE file for copyright and license details.
.POSIX:

include config.mk

SRC = zst.c x.c
OBJ = $(SRC:.c=.o)

all: options zst

options:
	@echo zst build options:
	@echo "CFLAGS  = $(STCFLAGS)"
	@echo "LDFLAGS = $(STLDFLAGS)"
	@echo "CC      = $(CC)"

config.h:
	cp config.def.h config.h

.c.o:
	$(CC) $(STCFLAGS) -c $<

zst.o: config.h zst.h win.h
x.o: arg.h zst.h win.h

$(OBJ): config.h config.mk

zst: $(OBJ)
	$(CC) -o $@ $(OBJ) $(STLDFLAGS)

clean:
	rm -f zst $(OBJ) zst-$(VERSION).tar.gz

dist: clean
	mkdir -p zst-$(VERSION)
	cp -R FAQ LEGACY TODO LICENSE Makefile README config.mk\
		config.def.h zst.info zst.1 arg.h zst.h win.h $(SRC)\
		st-$(VERSION)
	tar -cf - zst-$(VERSION) | gzip > zst-$(VERSION).tar.gz
	rm -rf zst-$(VERSION)

install: zst
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f zst $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/st
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < zst.1 > $(DESTDIR)$(MANPREFIX)/man1/st.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/st.1
	tic -sx zst.info
	@echo Please see the README file regarding the terminfo entry of zst.

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/zst
	rm -f $(DESTDIR)$(MANPREFIX)/man1/zst.1

.PHONY: all options clean dist install uninstall
